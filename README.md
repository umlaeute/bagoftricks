My Bag of Tricks for Debian packaging
=====================================


# simple upgrade fixes

Run the various `debfix-*` scripts to ensure consistent packaging.

The scripts try to detect whether they have already been run (or the changes were applied manually),
and exit early if so.
So they should be idempotent.

The scripts will attempt to commit their changes to the git repository
(with a consistent commit message), so only run them in an unmodified repo
(to not accidentally pick up unrelated changes).

To run all the scripts at once, i use something like the following `~/bin/debfix` script:

~~~sh
#!/bin/sh

for i in ~/bag-of-tricks/debfix-*; do
  $i
done
~~~

or in one line `for i in ~/bag-of-tricks/debfix-*; do $i; done`

### [`debfix`](debfix)

A sample implementation of the above.
You could either envoke this script as `~/bag-of-tricks/debfix`, or symlink
it and whatever debfix-scripts you want into `~/bin`, so you can simply run:

~~~sh
debfix
~~~


### [`debfix-add-salsa`](debfix-add-salsa)

Adds a `debian/salsa-ci.yml` file to the repository, that invokes the default Salsa-CI pipeline.

You still must tell salsa to actually use this file via the project's settings.

### [`debfix-rm-localoptions`](debfix-rm-localoptions)

Remove the `debian/source/local-options` file.

I once thought this file was a good idea, but no longer.
Esp. when using `dgit` to upload, the mere existence of this file is a show-stopper.

### [`debfix-rules-requires-root`](debfix-rules-requires-root)

Adds a `Rules-Requires-Root: no` stanza do *d/control* (unless there is already a `Rules-Requires-Root` stanza).


### [`debfix-rules-wrapandsort`](debfix-rules-wrapandsort)

Runs `wrap-and-sort -ast` for a consistent layout of d/control and friends.

### [`debfix-jack-over-jack2`](debfix-rules-wrapandsort)

Ensures that `libjack-dev` is preferred over `libjack-jackd2-dev` as (Build-)Dependency.

Ideally libjack-dev and libjack-jackd2-dev provide the same headers
(so it shouldn't really matter against which dev-package we build).

In the past, there have been differences, leading to programs that were
incompatible with jackd1 when compiled against libjack-jackd2-dev.
Programs compiled against libjack-dev (aka jackd1), have always been
compatible with jackd2.

If you think that this is no longer true, you probably should lobby for
libjack-dev to Depend on libjack-jackd2-dev and introduce a
libjack-jackd1-dev package.

The reason why people prefer `libjack-jackd2-dev` is, that on systems with
jackd2 installed (which is the preferred JACK implementation these days),
installing `libjack-dev` will try to remove the entire jackd2 stack, which
is of course annoying.
However, `debian/control` is mainly targetting the buildds, so it's their
need we have to cater for.

If you build packages locally (without using a build isolation like sbuild),
just install `libjack-jackd2-dev` *manually* *once*.


### [`debfix-bump-dh`](debfix-bump-dh)
Bump the `debhelper-compat` in *d/control* to the latest and greatest.
The actual debhelper compat level is hardcoded in the script (no automatic upgrades).


### [`debfix-bump-standardsversion`](debfix-bump-standardsversion)

Bump the `Standards-Version` in *d/control* to the latest and greatest.
The actual debhelper compat level is hardcoded in the script (no automatic upgrades).

Make sure that your packaging actually follows the latest standards version!

# no-change uploads to backports/ppa

Almost all of my packages that are available via
[Debian/backports](https://backports.debian.org) or
[Ubuntu/PPA](https://launchpad.net/~umlaeute) do not require any changes with
respect to their "parent" package (as uploaded to Debian/unstable).
For these, I think it is too much trouble to keep separate branches in git, so I
just do no-change rebuilds+uploads

### [`backports-nochange-buildput`](backports-nochange-buildput)

Creates a source(-only) package targetting at current backports and uploads it.

### [`ppa-buildput`](ppa-buildput)

Builds a source package for each of the listed Ubuntu-release and
attempts to upload it to a PPA via dput.
The default target releases are all currently supported (LTS) releases.
(With reasonable dependencies; at the time of writing (2022) this goes back
until bionic/18.04 LTS.)

The default PPA configuration for a package is derived from the package name.
E.g. for the "puredata" package, your dput-configuration
(`~/.config/dput/dput.cf`) should contain a section for `ppa-puredata`, like so:

```ini
[ppa-puredata]
fqdn = ppa.launchpad.net
method = ftp
incoming = ~pure-data/ubuntu/pure-data/
login = anonymous
allow_unsigned_uploads = 0
```

Change the `incoming` stanza to match your PPA archive.


# consistent clones

The [gbphook-postclone](gbphook-postclone) script makes sure that
git-repositories cloned with `gbp clone` have some properties I like.

namely:
- quilt's `.pc/` directory is ignored (otherwise 'gbp buildpackage' likes to complain if we just patched the sources)
- enable git's `--follow-tags` mode, so it's harder to forget to push (Debian and upstream release) tags

To automatically use this script whenever i run `gbp clone`, my `~/.gbp.conf` contains:

~~~ini
[clone]
postclone = ~/bag-of-tricks/gbphook-postclone
~~~


# gbp buildpackage

## pbuilder

I used to use `pbuilder` to build packages in a clean environment, but have started to love `sbuild` since.

To run a one-off build on `pbuilder` do:

~~~sh
git buildpackage --git-pbuilder=True
~~~

## sbuild


### dropping into a shell if something went wrong

~~~
--build-failed-commands='%SBUILD_SHELL'
~~~

(this is an `sbuild` option, but `gbp buildpackage` forwards all unknown-options to the builder)



### using build-profiles

To run a build with the 'nocheck' profile, add the following to the `gbp buildpackage` invocation:

~~~
--git-builder="DEB_BUILD_OPTIONS=nocheck sbuild" --profiles nocheck
~~~


- the `--profiles nocheck` is passed to the build command (via gbp-buildpackage's `OPTION_PASSED_TO_BUILD_CMD`).
- the `--git-builder` option is only required if you need to *also* set the `DEB_BUILD_OPTIONS` (which is recommended for the 'nocheck' profile)


# porterboxes


### available machines
a list of available machines for the various architectures can be found at
https://db.debian.org/machines.cgi


## setting up a chroot for building a specific package


execute the [`make-schroot`](make-schroot) script on the porterbox to create a schroot with build-dependencies installed.
the schroot will be named `umlaeute-<pkg>`. a working dir of the same name is created that contains:
- a `chroot.sh` script to enter the working directory in the context of the schroot
- a `teardown.sh` script to remove the entire schroot and the working dir
- the package sources (as obtained with `apt-get source`)
- (optionally) a clone of the packaging repository


### simple case

~~~sh
make-schroot puredata
~~~

- create a chroot based on the (default) `sid` image
- install the build-dependencies for the `puredata` package in the chroot
- unpack the sources for the `puredata` package in the working directory


### additionally clone the packaging repository
~~~sh
make-schroot https://salsa.debian.org/multimedia-team/pd/puredata.git
~~~

- create a chroot based on the (default) `sid` image
- install the build-dependencies for the `puredata` package in the chroot
- unpack the sources for the `puredata` package in the working directory
- clone the repository into the working directory

the package name used for installing the build-dependencies and source package is
simply the last component of the URL


### use another base image

~~~sh
make-schroot puredata experimental
~~~

Does the same as the simple case, but uses the `experimental` schroot as the base.

To get list of available schroots, run:

~~~sh
schroot --list | sed -e 's|^chroot:||' -e '/:/d'
~~~
